# cloud-practice

#### Description
SpringCloud Practice Repositories

#### Branches

master branch is a content list, other branchs are for each component of SpringCloud practice 
|Branch Names | Links | CreateTime| Notes | 
| ----- | ----- | ---- | ---- | 
| eureka-test  | https://gitee.com/devilscode/cloud-practice/tree/eureka-test/ | 2019-10-05 |  |
| ribbon-test  | https://gitee.com/devilscode/cloud-practice/tree/ribbon-test/ | 2019-10-09 |  |
| feign-test   | https://gitee.com/devilscode/cloud-practice/tree/feign-test/ | 2019-10-22  |  |
| hystrix-test | https://gitee.com/devilscode/cloud-practice/tree/hystrix-test/ | 2019-10-24|  |
| zuul-test	| https://gitee.com/devilscode/cloud-practice/tree/zuul-test/ | 2019-10-24|  |
| config-test | https://gitee.com/devilscode/cloud-practice/tree/config-test/ | 2019-11-19|  |

#### Refer Blog

##### [Series](https://my.oschina.net/devilsblog?tab=newest&catalogId=6547207)

##### Details
1. [Meeting Micro service ](https://my.oschina.net/devilsblog/blog/3112715) Know about each component and their functions
2. [Spring Cloud Eureka](https://my.oschina.net/devilsblog/blog/3114468) Highly Register Center
3. [Spring Cloud Ribbon](https://my.oschina.net/devilsblog/blog/3115061) Consumer And LoadBalance
4. [Spring Cloud Feign](https://my.oschina.net/devilsblog/blog/3120790) Called another service like make a statement
5. [Spring Cloud Hystrix](https://my.oschina.net/devilsblog/blog/3121826) Hystrix, The solution of Error in services
6. [Spring Cloud Zuul](https://my.oschina.net/devilsblog/blog/3122600)  Monitor and Router
7. [Spring Cloud Config](https://my.oschina.net/devilsblog/blog/3131448) Highly Configuration Center
