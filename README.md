# cloud-practice

#### 介绍
SpringCloud练习仓库

#### 分支
master分支只为介绍使用，其余各个分支名称即为每个组件的探索

|分支名称 | 地址 | 创建时间| 备注 | 
| ----- | ----- | ---- | ---- | 
| eureka-test  | https://gitee.com/devilscode/cloud-practice/tree/eureka-test/ | 2019-10-05 |  |
| ribbon-test  | https://gitee.com/devilscode/cloud-practice/tree/ribbon-test/ | 2019-10-09 |  |
| feign-test   | https://gitee.com/devilscode/cloud-practice/tree/feign-test/ | 2019-10-22  |  |
| hystrix-test | https://gitee.com/devilscode/cloud-practice/tree/hystrix-test/ | 2019-10-24|  |
| zuukl-test | https://gitee.com/devilscode/cloud-practice/tree/zuul-test/ | 2019-10-27|  |
| config-test | https://gitee.com/devilscode/cloud-practice/tree/config-test/ | 2019-11-19|  |

#### 关联博客

##### [系列地址](https://my.oschina.net/devilsblog?tab=newest&catalogId=6547207)

##### 明细
1. [初识微服务之功能篇](https://my.oschina.net/devilsblog/blog/3112715) 微服务功能之通读
2. [Spring Cloud Eureka](https://my.oschina.net/devilsblog/blog/3114468) 高可用服务注册中心
3. [Spring Cloud Ribbon](https://my.oschina.net/devilsblog/blog/3115061) 微服务消费之负载均衡
4. [Spring Cloud Feign](https://my.oschina.net/devilsblog/blog/3120790) 声明式调用Java Http客户端
5. [Spring Cloud Hystrix](https://my.oschina.net/devilsblog/blog/3121826) 熔断器，故障解决方案
6. [Spring Cloud Zuul](https://my.oschina.net/devilsblog/blog/3122600) 网关监控
7. [Spring Cloud Config](https://my.oschina.net/devilsblog/blog/3131448) 高可用配置中心


